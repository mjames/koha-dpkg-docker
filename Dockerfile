FROM debian:buster-slim

LABEL maintainer="tomascohen@theke.io"

# Valid: master, major.minor (19.11, etc)
ARG BRANCH

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin:/
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && apt-get -y --allow-unauthenticated install wget gnupg ca-certificates

# Add Node.js
RUN wget -O- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN wget -O- -q https://deb.nodesource.com/setup_14.x | bash -
# Pin Node.js v14
RUN echo "Package: nodejs"   >> /etc/apt/preferences.d/nodejs
RUN echo "Pin: version 14.*" >> /etc/apt/preferences.d/nodejs
RUN echo "Pin-Priority: 999" >> /etc/apt/preferences.d/nodejs
# Add yarn repo
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN wget -O- -q https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null
RUN echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get -y update \
    && apt-get -y --allow-unauthenticated install \
      bash-completion \
      build-essential \
      debian-archive-keyring \
      devscripts \
      dh-make \
      docbook-xsl-ns \
      fakeroot \
      git \
      gnupg \
      gnupg2 \
      libmodern-perl-perl \
      pbuilder \
      wget \
      nodejs \
      yarn \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/api/lists/*

RUN mkdir -p /var/cache/pbuilder/
# COPY base.tgz /var/cache/pbuilder/base.tgz

VOLUME /debs
VOLUME /koha
VOLUME /output

ENV PERL5LIB=/koha

COPY pbuilder.sh /pbuilder.sh
ADD build.sh /build.sh

CMD /build.sh
